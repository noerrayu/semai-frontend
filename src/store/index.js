import Vue from 'vue'
import Vuex from 'vuex'
import ProductsModule from '../store/modules/product-module'
import UserModule from '../store/modules/user-module'
import AccountModule from '../store/modules/account'
import CartModule from '../store/modules/cart-module'
import FarmerModule from '@/store/modules/farmer-module'
import ReviewModule from '@/store/modules/review-module'
import OrderModule from '@/store/modules/order-module'
import SearchModule from '@/store/modules/search-module'

Vue.use(Vuex)

export default new Vuex.Store({
  // untuk login
  state: {
    status: '',
    token: localStorage.getItem('token') || '',
    _id: localStorage.getItem('_id') || '',
    user: {}
  },

  // untuk mengambil state login
  getters: {
    isLoggedIn: state => !!state.token,
    authStatus: state => state.status,
  },

  // mengubah state dengan action dari user-module.js
  mutations: {
    auth_request(state) {
      state.status = 'loading'
    },
    auth_success(state, token, user) {
      state.status = 'success'
      state.token = token
      state.user = user
    },
    auth_error(state) {
      state.status = 'error'
    },
    logout(state) {
      state.status = ''
      state.token = ''
      state._id = ''
    },
  },

  actions: {
  },

  // memasang modul
  modules: {
    ProductsModule,
    UserModule,
    AccountModule,
    CartModule,
    FarmerModule,
    ReviewModule,
    OrderModule,
    SearchModule,
  }
})
