import axios from 'axios'

// mendefinisikan state awal
// state merupakan suatu object berisi semua data aplikasi berada 
const state = {
    products: [], //membuat array untuk menyimpan semua product
    productsbyfid: [], //membuat array untuk menyimpan semua product by farmer id
    productsHarvestedFalse: [],
    productsHarvestedTrue: [],
    product: {}, //membuat object product untuk menyimpan satu product/ detail produk
    isLoading: false, //mendefinisikan kondisi state isloading untuk pertamakali
};

// getter digunakan untuk mengakses state, seperti fungsi computed kalau tidak menggunakan vuex
const getters = {
    allProducts: state => state.products, //mengakses data semua product pada state array products
    allProductsbyfid: state => state.productsbyfid, //mengakses data semua product pada state array products
    allProductsHarvestedFalse: state => state.productsHarvestedFalse,
    allProductsHarvestedTrue: state => state.productsHarvestedTrue,
    detailProduct: state => state.product, //mengakses detail product yang disimpan pada state product
    isLoading: state => state.isLoading, //mengakses kondisi state isLoading
};

// action akan memanggil fungsi pada mutation untuk merubah state
// hampir sama seperti mutation tetapi action bisa digunakan secara asynchronous sedangkan mutation bekerja secara synchronous
// biasanya berisikan API call
const actions = {
    //get semua product
    async fetchProducts({ commit }) {
        // memanggil action setLoading dan mengubah kondisi menjadi true sebelum memanggil method GET API
        commit('setLoading', true)
        await axios.get("https://semai.herokuapp.com/api/product")
            .then(res => {
                commit('setProducts', res.data);
                commit('setLoading', false); // setelah meload data mengubah kondisi setLoading menjadi false
                console.log(res.data)
            }).catch(err => {
                console.log('error', err);
                commit('setLoading', false);
            })
    },

    //by harvested or not value
    async fetchProductsHarvestedFalse({ commit }) {
        // memanggil action setLoading dan mengubah kondisi menjadi true sebelum memanggil method GET API
        commit('setLoading', true)
        await axios.get("https://semai.herokuapp.com/api/product/harvested/false")
            .then(res => {
                commit('setProductsHarvestedFalse', res.data);
                commit('setLoading', false); // setelah meload data mengubah kondisi setLoading menjadi false
                console.log(res.data)
            }).catch(err => {
                console.log('error', err);
                commit('setLoading', false);
            })
    },

    async fetchProductsHarvestedTrue({ commit }) {
        // memanggil action setLoading dan mengubah kondisi menjadi true sebelum memanggil method GET API
        commit('setLoading', true)
        await axios.get("https://semai.herokuapp.com/api/product/harvested/true")
            .then(res => {
                commit('setProductsHarvestedTrue', res.data);
                commit('setLoading', false); // setelah meload data mengubah kondisi setLoading menjadi false
                console.log(res.data)
            }).catch(err => {
                console.log('error', err);
                commit('setLoading', false);
            })
    },

    //get semua product sesuai dengan id farmer  
    async fetchProductsbyfid({ commit }, id) {
        commit('setLoading', true)
        await axios.get(`https://semai.herokuapp.com/api/product/farmer/${id}`)
            .then(res => {
                commit('setProductsbyfid', res.data);
                let productsFid = state.productsbyfid.filter(item => {
                    return item.harvest.nowShowing === true
                })
                // commit('setProductsbyfid', res.data);
                commit('setLoading', false);
                console.log(res.data)
                if (productsFid) {
                    commit('setProductsbyfid', productsFid)
                }
            }).catch(err => {
                console.log('error', err);
                commit('setLoading', false);
            })
    },

    //get semua product dengan filter category
    async fetchProductsFilter({ commit }, category) {
        commit('setLoading', true)
        await axios.get(`https://semai.herokuapp.com/api/product/filter/${category}`)
            .then(res => {
                commit('setProducts', res.data);
                commit('setLoading', false);
                console.log(res.data)
            }).catch(err => {
                console.log('error', err);
                commit('setLoading', false);
            })
    },

    //get detail product by id, dapat id dari params di ProductDesc.vue
    async fetchProductDetail({ commit }, id) {
        commit('setLoading', true)
        await axios.get(`https://semai.herokuapp.com/api/product/${id}`)
            .then(res => {
                commit('setProductDetail', res.data);
                commit('setLoading', false);
                console.log(res.data)
            }).catch(err => {
                console.log('error', err);
                commit('setLoading', false);
            })
    },
};

// mutation berisikan cara-cara yang dilakukan untuk merubah state
const mutations = {
    // membuat fungsi setProducts yang akan dipanggil action untuk menyimpan data pada state array products
    setProducts: (state, products) => (
        state.products = products
    ),

    // sama dengan setProducts tetapi berdasarkan farmer id
    setProductsbyfid: (state, productsbyfid) => {
        state.productsbyfid = productsbyfid
    },

    // setProductDetail untuk menyimpan detail product pada state product
    setProductDetail: (state, product) => {
        state.product = product
    },

    // setLoading untuk mengubah kondisi state loading true atau false
    setLoading: (state, isLoading) => {
        state.isLoading = isLoading
    },

    // berdasarkan produk memiliki nilai harvested: false
    setProductsHarvestedFalse: (state, productsHarvestedFalse) => {
        state.productsHarvestedFalse = productsHarvestedFalse
    },

    // berdasarkan produk memiliki nilai harvested: true
    setProductsHarvestedTrue: (state, productsHarvestedTrue) => {
        state.productsHarvestedTrue = productsHarvestedTrue
    },
};

export default {
    state,
    getters,
    actions,
    mutations
}
