import axios from 'axios'

const state = {
    cart: [],
    payment: ''
};


const getters = {
    allCart: state => state.cart,
    //mengitung total jenis produk
    cartItemTotal: (state) => state.cart.length,
    //mengitung total harga
    cartPriceTotal: (state) => {
        let total = 0; state.cart.forEach(item => {
            total += item.product.price * item.qty;
        })
        return total;
    },
    //menghitung total produk
    cartItemsTotal: (state) => {
        let total = 0; state.cart.forEach(item => {
            total += item.qty;
        })
        return total;
    }
};


const actions = {
    // memasukkan ke keranjang/cart
    async addToCart({ commit, state }, { product, quantity }) {
        let user = localStorage._id;
        //kondisi kalau udah ada id product yang pernah ditambahin gausah post baru tapi di put quantitynya
        let productInCart = state.cart.find(item => {
            return item.product._id === product._id;
        });
        if (productInCart) {
            await axios.put(`https://semai.herokuapp.com/api/cart/${productInCart._id}`, {
                product: productInCart.product._id,
                qty: productInCart.qty + 1,
                user: user
            })
                .then(res => {
                    console.log(res)
                }).catch(err => {
                    console.log('error', err);
                })
            return;
        } else {
            commit('Add_To_Cart', { product, quantity });
            await axios.post('https://semai.herokuapp.com/api/cart', {
                product: product._id,
                qty: quantity,
                farmer: product.farmer,
                user: user
            })
                .then(res => {
                    console.log(res)
                }).catch(err => {
                    console.log('error', err);
                })
        }
    },

    // mengambil semua cart sesuai id user
    async getAllCart({ commit }) {
        let user = localStorage._id;
        await axios.get(`https://semai.herokuapp.com/api/cart/user/${user}`)
            .then(res => {
                commit('Set_Cart', res.data);
                let productInCart = state.cart.filter(item => {
                    return item.payment.isPaid === false //changed
                })
                console.log("cart", productInCart)
                if (!productInCart) {
                    commit('Set_Cart',)
                } else {
                    commit('Set_Cart', productInCart)
                }
            }).catch(err => {
                console.log('error', err);
            })
    },

    //menambahkan jumlah produk
    async addQty({ commit }, { product, id, quantity }) {
        commit("Add_Qty", id);
        await axios.put(`https://semai.herokuapp.com/api/cart/${id}`, {
            product: product,
            qty: quantity,
            user: localStorage._id
        })
            .then(res => {
                console.log(res)
            }).catch(err => {
                console.log('error', err);
            })
    },

    //mengurangi jumlah produk
    async reduceQty({ commit }, { product, id, quantity }) {
        commit("Reduce_Qty", id)
        await axios.put(`https://semai.herokuapp.com/api/cart/${id}`, {
            product: product,
            qty: quantity,
            user: localStorage._id
        })
            .then(res => {
                console.log(res)
            }).catch(err => {
                console.log('error', err);
            })
    },

    // menghapus produk dari cart
    async deleteCart({ commit }, id) {
        commit("Reduce_Qty", id)
        await axios.delete(`https://semai.herokuapp.com/api/cart/${id}`)
            .then(res => {
                location.reload();
                console.log(res);
            }).catch(err => {
                console.log('error', err);
            })
    },

    // mengirim isi cart ke halaman pesanan
    async addToPayment({ commit, state }, { paymentMethod, subscription }) {
        console.log("payment", paymentMethod, subscription);
        commit('Add_To_Payment', { paymentMethod, subscription });
        let productInCart = state.cart;
        await axios.all(productInCart.map(item => {
            return axios.put(`https://semai.herokuapp.com/api/cart/${item._id}`, {
                product: item.product._id,
                user: localStorage._id,
                payment: {
                    isPaid: true, //changed
                    paymentMethod: paymentMethod,
                    subscription: subscription
                },
                delivery: {
                    isAccepted: false,
                    isDelivery: false,
                    isDone: false //changed
                },
            })
                .then(res => {
                    location.reload();
                    console.log(res);
                }).catch(err => {
                    console.log('error', err);
                })
        }))
            .then(res => {
                console.log(res);
            }).catch(err => {
                console.log('error', err);
            })
    },
};


const mutations = {
    Add_To_Cart: (state, { product, quantity }) => {
        //menambahkan logic increment product pada id product yang sama
        let productInCart = state.cart.find(item => {
            return item.product._id === product._id;
        });
        if (productInCart) {
            productInCart.quantity += quantity;
            return;
        }
        state.cart.push({
            product,
            quantity
        })
    },

    Set_Cart: (state, allCart) => {
        state.cart = allCart;
    },

    Add_Qty: (state, id) => {
        let productInCart = state.cart.find((item) => item._id === id);
        productInCart.qty++;
    },

    Reduce_Qty: (state, id) => {
        let productInCart = state.cart.find((item) => item._id === id);

        if (productInCart.qty > 1) {
            productInCart.qty--;
        } else {
            state.cart = state.cart.filter((item) => item.product._id !== id)
        }
    },

    Add_To_Payment: (state, { quantity, totalPay }) => (
        state.payment = totalPay, quantity
    ),
};

export default {
    state,
    getters,
    actions,
    mutations
}
