import axios from 'axios'

// mendefinisikan state awal
// state merupakan suatu object berisi semua data aplikasi berada 
const state = {
    farmers: [],
    farmer: {}, //menyimpan data farmer sesuai id
    summary: {}, //membuat object product untuk menyimpan satu product/ detail produk
};

// getter digunakan untuk mengakses state, seperti fungsi computed kalau tidak menggunakan vuex
const getters = {
    dataFarmers: state => state.farmers,
    dataFarmer: state => state.farmer,
    dataSummary: state => state.summary, //mengakses detail product yang disimpan pada state product
};

// action akan memanggil fungsi pada mutation untuk merubah state
// hampir sama seperti mutation tetapi action bisa digunakan secara asynchronous sedangkan mutation bekerja secara synchronous
// biasanya berisikan API call
const actions = {
    async fetchAllFarmer({ commit }) {
        commit('setLoading', true)
        await axios.get(`https://semai.herokuapp.com/api/farmer`)
            .then(res => {
                commit('setFarmers', res.data);
                commit('setLoading', false);
                console.log(res.data)
            }).catch(err => {
                console.log('error', err);
                commit('setLoading', false);
            })
    },

    async fetchFarmerData({ commit }, id) {
        commit('setLoading', true)
        await axios.get(`https://semai.herokuapp.com/api/farmer/${id}`)
            .then(res => {
                commit('setFarmerData', res.data);
                commit('setLoading', false);
                console.log(res.data)
            }).catch(err => {
                console.log('error', err);
                commit('setLoading', false);
            })
    },

    async fetchSummaryData({ commit }, id) {
        commit('setLoading', true)
        await axios.get(`https://semai.herokuapp.com/api/farmer/summary/${id}`)
            .then(res => {
                commit('setSummaryData', res.data);
                commit('setLoading', false);
                console.log(res.data)
            }).catch(err => {
                console.log('error', err);
                commit('setLoading', false);
            })
    },
};

// mutation berisikan cara-cara yang dilakukan untuk merubah state
const mutations = {
    setFarmers: (state, farmers) => {
        state.farmers = farmers
    },

    setFarmerData: (state, farmer) => {
        state.farmer = farmer
    },

    setSummaryData: (state, summary) => {
        state.summary = summary
    },
};

export default {
    state,
    getters,
    actions,
    mutations
}
