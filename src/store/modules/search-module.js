import axios from 'axios'

const state = {
    search: [],
    searchFarmer: [],
};

const getters = {
    currentSearch: state => state.search,
    currentSearchFarmer: state => state.searchFarmer,
};

const actions = {
    // search produk
    async fetchSearchValue({ commit }, searchValue) {
        commit('setLoading', true)
        await axios.get(`https://semai.herokuapp.com/api/product/search/${searchValue}`)
            .then(res => {
                commit('setSearch', res.data);
                commit('setLoading', false);
                console.log(res.data)
            }).catch(err => {
                console.log('error', err);
                commit('setLoading', false);
            })
    },

    // search farmer
    async fetchSearchValueFarmer({ commit }, searchValue) {
        commit('setLoading', true)
        await axios.get(`https://semai.herokuapp.com/api/farmer/search/${searchValue}`)
            .then(res => {
                commit('setSearchFarmer', res.data);
                commit('setLoading', false);
                console.log(res.data)
            }).catch(err => {
                console.log('error', err);
                commit('setLoading', false);
            })
    },
};

const mutations = {
    setSearch: (state, searchValue) => (
        state.search = searchValue
    ),
    setSearchFarmer: (state, searchValue) => (
        state.searchFarmer = searchValue
    ),
};

export default {
    state,
    getters,
    actions,
    mutations
}
